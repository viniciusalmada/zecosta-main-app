package com.viniciusalmada.tocomzecosta.utils;

import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

public class ViewsUtils {
    public static void setViewsDisabled(View... views) {
        for (View v : views) {
            v.setEnabled(false);
        }
    }

    public static void setViewsEnabled(View... views) {
        for (View v : views) {
            v.setEnabled(true);
        }
    }

    public static void setViewsClearAndEnabled(View... views) {
        for (View v : views) {
            v.setEnabled(true);
            if (v instanceof EditText)
                ((EditText) v).setText("");
            else if (v instanceof Spinner)
                ((Spinner) v).setSelection(0);
        }
    }

    public static void setViewsClear(View... views) {
        for (View v : views) {
            if (v instanceof EditText)
                ((EditText) v).setText("");
            else if (v instanceof Spinner)
                ((Spinner) v).setSelection(0);
        }
    }
}
