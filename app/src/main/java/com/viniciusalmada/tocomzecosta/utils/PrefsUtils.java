package com.viniciusalmada.tocomzecosta.utils;

import android.content.Context;
import android.content.SharedPreferences;

import static com.viniciusalmada.tocomzecosta.activities.RegisterActivity.SPACE;

public class PrefsUtils {
    public static final String KEY_NOME_STRING = "KEY_NOME_STRING";
    public static final String KEY_EMAIL_STRING = "KEY_EMAIL_STRING";
    public static final String KEY_CITY_INDEX_INT = "KEY_CITY_INDEX_INT";
    public static final String KEY_SUBJECT_INDEX_INT = "KEY_SUBJECT_INDEX_INT";
    public static final String KEY_AGE_INT = "KEY_AGE_INT";
    public static final String KEY_GENDER_INDEX = "KEY_GENDER_INDEX";
    public static final String KEY_DDD_INT = "KEY_DDD_INT";
    public static final String KEY_PHONE_LONG = "KEY_PHONE_LONG";
    public static final String KEY_TITULO_LONG = "KEY_TITULO_LONG";
    public static final String KEY_SECAO_LONG = "KEY_SECAO_INT";
    public static final String KEY_ZONA_LONG = "KEY_ZONA_INT";
    public static final String KEY_REGISTER_IS_DONE_BOOLEAN = "KEY_REGISTER_IS_DONE_BOOLEAN";
    public static final String KEY_UUID = "KEY_UUID";
    public static final String KEY_TIMESTAMP = "KEY_TIMESTAMP";
    private static final String PREF_ID = "zecosta";

    public static void setBoolean(Context context, String key, boolean value) {
        SharedPreferences.Editor ed = getSharedPrefsEditor(context);
        ed.putBoolean(key, value);
        ed.commit();
    }

    public static void setLong(Context context, String key, long value) {
        SharedPreferences.Editor ed = getSharedPrefsEditor(context);
        ed.putLong(key, value);
        ed.commit();
    }

    public static void setInt(Context context, String key, int value) {
        SharedPreferences.Editor ed = getSharedPrefsEditor(context);
        ed.putInt(key, value);
        ed.commit();
    }

    public static void setString(Context context, String key, String value) {
        SharedPreferences.Editor ed = getSharedPrefsEditor(context);
        ed.putString(key, value);
        ed.commit();
    }

    public static boolean getBoolean(Context context, String key) {
        SharedPreferences preferences = getSharedPrefs(context);
        return preferences.getBoolean(key, false);
    }

    public static long getLong(Context context, String key) {
        SharedPreferences prefs = getSharedPrefs(context);
        return prefs.getLong(key, -1L);
    }

    public static int getInt(Context context, String key) {
        SharedPreferences prefs = getSharedPrefs(context);
        return prefs.getInt(key, -1);
    }

    public static String getString(Context context, String key) {
        SharedPreferences prefs = getSharedPrefs(context);
        return prefs.getString(key, SPACE);
    }

    private static SharedPreferences.Editor getSharedPrefsEditor(Context context) {
        return context.getSharedPreferences(PREF_ID, 0).edit();
    }

    private static SharedPreferences getSharedPrefs(Context context) {
        return context.getSharedPreferences(PREF_ID, 0);
    }
}
