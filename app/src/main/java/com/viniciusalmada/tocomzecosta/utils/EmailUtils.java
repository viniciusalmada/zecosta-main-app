package com.viniciusalmada.tocomzecosta.utils;

public class EmailUtils {
    private static int indexOfAt(String email) {
        if (email.indexOf('@') > 0)
            return email.indexOf('@');
        else
            return -1;
    }

    public static boolean checkEmail(String email){

        if (email.trim().length() != email.length()){
            return false;
        }

        int at = indexOfAt(email);
        if (at > 0){
            return true;
        }

        return false;
    }
}
