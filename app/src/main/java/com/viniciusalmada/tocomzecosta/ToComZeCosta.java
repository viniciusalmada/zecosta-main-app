package com.viniciusalmada.tocomzecosta;

import android.app.Application;

import com.facebook.stetho.Stetho;

public class ToComZeCosta extends Application {
    public static final String TAG = "zeze";

    @Override
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
