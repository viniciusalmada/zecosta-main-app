package com.viniciusalmada.tocomzecosta.fragments;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.button.MaterialButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;

import com.google.android.gms.tasks.Task;
import com.viniciusalmada.tocomzecosta.R;
import com.viniciusalmada.tocomzecosta.activities.MainActivity;
import com.viniciusalmada.tocomzecosta.domains.Sugestao;
import com.viniciusalmada.tocomzecosta.utils.EmailUtils;
import com.viniciusalmada.tocomzecosta.utils.NetworkUtils;
import com.viniciusalmada.tocomzecosta.utils.PrefsUtils;
import com.viniciusalmada.tocomzecosta.utils.ToastUtils;
import com.viniciusalmada.tocomzecosta.utils.ViewsUtils;

import java.util.Objects;
import java.util.UUID;

import static com.viniciusalmada.tocomzecosta.activities.RegisterActivity.DDD_LENGTH;
import static com.viniciusalmada.tocomzecosta.activities.RegisterActivity.PHONE_LENGHT;
import static com.viniciusalmada.tocomzecosta.activities.RegisterActivity.SPACE;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.KEY_CITY_INDEX_INT;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.KEY_EMAIL_STRING;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.KEY_NOME_STRING;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.KEY_SUBJECT_INDEX_INT;


public class SugestaoFragment extends Fragment {

    private View mView;
    private EditText mEtNome;
    private EditText mEtEmail;
    private EditText mEtMessage;
    private Spinner mSpCities;
    private Spinner mSpSubjects;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_sugestao, container, false);
        initViews();

        MaterialButton sendButton = mView.findViewById(R.id.bt_sug_send);
        sendButton.setOnClickListener(view -> onSendButtonClick());

        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() != null)
            ((MainActivity) getActivity()).setToolbarTitle(R.string.sugestao);
    }

    @Override
    public void onResume() {
        super.onResume();
        initViewsFromPrefs();
    }

    // Initial Commands

    private void initViews() {
        mEtNome = mView.findViewById(R.id.et_sug_nome);
        mEtEmail = mView.findViewById(R.id.et_sug_email);
        mEtMessage = mView.findViewById(R.id.et_sug);
        mSpCities = mView.findViewById(R.id.sp_sug_cities);
        mSpSubjects = mView.findViewById(R.id.sp_sug_subject);
    }

    private void initViewsFromPrefs() {
        String nomePrefs = PrefsUtils.getString(getActivity(), PrefsUtils.KEY_NOME_STRING);
        String emailPrefs = PrefsUtils.getString(getActivity(), PrefsUtils.KEY_EMAIL_STRING);
        int cityIndexPrefs = PrefsUtils.getInt(getActivity(), PrefsUtils.KEY_CITY_INDEX_INT);
        int subjectIndexPrefs = PrefsUtils.getInt(getActivity(), PrefsUtils.KEY_SUBJECT_INDEX_INT);

        if (!nomePrefs.equals(SPACE)) mEtNome.setText(nomePrefs);
        if (!emailPrefs.equals(SPACE)) mEtEmail.setText(emailPrefs);
        if (cityIndexPrefs != -1) mSpCities.setSelection(cityIndexPrefs);
        if (subjectIndexPrefs != -1) mSpSubjects.setSelection(subjectIndexPrefs);
    }

    // Listeners

    private void onSendButtonClick() {
        showProgress();

        hideErrorsOfTextInputLayouts();

        String nome = checkEditTextAndGetContent(mEtNome, R.id.til_sug_nome);
        String email = checkEditTextAndGetContent(mEtEmail, R.id.til_sug_email);
        String message = checkEditTextAndGetContent(mEtMessage, R.id.til_sug);
        String city = getCityOfSpinner(mSpCities);
        String subject = getSubjectOfSpinner(mSpSubjects);
        String uid = UUID.randomUUID().toString();
        long timestamp = System.currentTimeMillis();

        if (!nome.equals(SPACE) && !email.equals(SPACE) && !message.equals(SPACE)) {
            ViewsUtils.setViewsDisabled(mEtNome, mEtEmail, mEtMessage, mSpCities, mSpSubjects);

            Sugestao sugestao = getSugestao(uid, nome, email, city, subject, message, timestamp);
            if (NetworkUtils.isNetworkAvailable(getActivity())) {
                sugestao.saveOnFirebase().addOnCompleteListener(task -> onSaveSuggestion(task, sugestao));
            } else {
                ViewsUtils.setViewsEnabled(mEtNome, mEtEmail, mEtMessage, mSpCities, mSpSubjects);
                ToastUtils.show(getActivity(), "Erro! Verifique sua conexão e tente mais tarde!");
                hideProgress();
            }
        } else {
            hideProgress();
        }
    }

    private void onSaveSuggestion(Task<Void> task, Sugestao sugestao) {
        if (task.isSuccessful()) {
            ViewsUtils.setViewsClearAndEnabled(mEtNome, mEtEmail, mEtMessage, mSpCities, mSpSubjects);
            ToastUtils.show(getActivity(), "Sugestão enviada, obrigado!");
            PrefsUtils.setString(getActivity(), KEY_NOME_STRING, sugestao.getNome());
            PrefsUtils.setString(getActivity(), KEY_EMAIL_STRING, sugestao.getEmail());
            PrefsUtils.setInt(getActivity(), KEY_CITY_INDEX_INT, mSpCities.getSelectedItemPosition());
            PrefsUtils.setInt(getActivity(), KEY_SUBJECT_INDEX_INT, mSpSubjects.getSelectedItemPosition());
            ((MainActivity) Objects.requireNonNull(getActivity())).openDrawer();
        } else {
            ViewsUtils.setViewsEnabled(mEtNome, mEtEmail, mEtMessage, mSpCities, mSpSubjects);
            ToastUtils.show(getActivity(), "Erro! Verifique sua conexão e tente mais tarde!");
        }
        hideProgress();
    }

    // Progress Visibility Controls

    private void showProgress() {
        mView.findViewById(R.id.progress).setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        mView.findViewById(R.id.progress).setVisibility(View.INVISIBLE);
    }

    // Checkers and Getters from Form

    private String checkEditTextAndGetContent(EditText field, @IdRes int idTextInputLayout) {
        if (field.getText().toString().trim().length() != 0) {
            return field.getText().toString();
        } else {
            showErrorEditText(idTextInputLayout);
            return SPACE;
        }
    }

    private String checkEmailEditTextAndGetContent(EditText emailField, int idTextInputLayout) {
        String email = checkEditTextAndGetContent(emailField, idTextInputLayout);
        if (!email.equals(SPACE) && !EmailUtils.checkEmail(email)) {
            showErrorEditText(idTextInputLayout, "Email incorreto");
            return SPACE;
        }
        if (!email.equals(SPACE)) {
            PrefsUtils.setString(getActivity(), KEY_EMAIL_STRING, email);
            return email;
        }
        return SPACE;
    }

    private String checkDDDEditTextAndGetContent(EditText dddField, int idTextInputLayout) {
        String ddd = checkEditTextAndGetContent(dddField, idTextInputLayout);
        if (!ddd.equals(SPACE) && ddd.length() != DDD_LENGTH) {
            showErrorEditText(idTextInputLayout, "DDD incompleto");
            return SPACE;
        } else if (!ddd.equals(SPACE) && ddd.length() == DDD_LENGTH) {
            return ddd;
        } else {
            return SPACE;
        }
    }

    private String checkPhoneEditTextAndGetContent(EditText phoneField, int idTextInputLayout) {
        String phone = checkEditTextAndGetContent(phoneField, idTextInputLayout);
        if (!phone.equals(SPACE) && phone.length() != PHONE_LENGHT) {
            showErrorEditText(idTextInputLayout, "Número incompleto");
            return SPACE;
        } else if (!phone.equals(SPACE) && phone.length() == PHONE_LENGHT) {
            return phone;
        } else {
            return SPACE;
        }
    }

    private String getCityOfSpinner(Spinner spCity) {
        if (getActivity() != null) {
            String[] cities = getActivity().getResources().getStringArray(R.array.maranhao_cities);
            return cities[spCity.getSelectedItemPosition()];
        }
        return SPACE;
    }

    private String getSubjectOfSpinner(Spinner spSubjects) {
        if (getActivity() != null) {
            String[] cities = getActivity().getResources().getStringArray(R.array.subjects_suggestions);
            return cities[spSubjects.getSelectedItemPosition()];
        }
        return SPACE;
    }

    // Erros Controls

    private void hideErrorsOfTextInputLayouts() {
        ((TextInputLayout) mView.findViewById(R.id.til_sug_nome)).setErrorEnabled(false);
        ((TextInputLayout) mView.findViewById(R.id.til_sug_email)).setErrorEnabled(false);
        ((TextInputLayout) mView.findViewById(R.id.til_sug)).setErrorEnabled(false);
    }

    private void showErrorEditText(@IdRes int idTextInputLayout) {
        TextInputLayout til = mView.findViewById(idTextInputLayout);
        til.setError(getString(R.string.error_text));
    }

    private void showErrorEditText(@IdRes int idTextInputLayout, String error) {
        TextInputLayout til = mView.findViewById(idTextInputLayout);
        til.setError(error);
    }

    private Sugestao getSugestao(String uid, String nome, String email, String city, String subject, String message, long timestamp) {
        return new Sugestao(uid, nome, email, city, subject, message, timestamp);
    }
}
