package com.viniciusalmada.tocomzecosta.domains;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.IgnoreExtraProperties;

public class Apoiador {
    private String cidade;
    private String email;
    private long phone;
    private long zona;
    private long section;
    private long titulo;
    private String nome;
    private int idade;
    private String sexo;
    private String uid;
    private long timestamp;

    public Apoiador() {
    }

    public Apoiador(String cidade, String email, long phone, long zona, long section, long titulo,
                    String nome, int idade, String sexo, String uid, long timestamp) {
        this.cidade = cidade;
        this.email = email;
        this.phone = phone;
        this.zona = zona;
        this.section = section;
        this.titulo = titulo;
        this.nome = nome;
        this.idade = idade;
        this.sexo = sexo;
        this.uid = uid;
        this.timestamp = timestamp;
    }

    public String getCidade() {
        return cidade;
    }

    public String getEmail() {
        return email;
    }

    public long getPhone() {
        return phone;
    }

    public long getZona() {
        return zona;
    }

    public long getSection() {
        return section;
    }

    public long getTitulo() {
        return titulo;
    }

    public String getNome() {
        return nome;
    }

    public int getIdade() {
        return idade;
    }

    public String getSexo() {
        return sexo;
    }

    public String getUid() {
        return uid;
    }

    public long getTimestamp() {
        return timestamp;
    }

    @Exclude
    public Task<Void> saveOnFirebase() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("apoiadores");
        return ref.child(timestamp + "_" + uid).setValue(this);
    }

    @Exclude
    public Task<Void> updateOnFirebase(long oldTimestamp) {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("apoiadores");
        DatabaseReference oldNode = ref.child(oldTimestamp + "_" + uid);
        oldNode.removeValue();
        return ref.child(timestamp + "_" + uid).setValue(this);
    }

    @Exclude
    public int getDDD() {
        String sPhone = String.valueOf(this.phone);
        String sDDD = sPhone.substring(0,2);
        return Integer.parseInt(sDDD);
    }

    @Exclude
    public long getPhoneNumber() {
        String sPhone = String.valueOf(this.phone);
        String sNumber = sPhone.substring(2,11);
        return Long.parseLong(sNumber);

    }
}
