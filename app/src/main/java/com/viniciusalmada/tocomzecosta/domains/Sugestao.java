package com.viniciusalmada.tocomzecosta.domains;

import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class Sugestao {
    private String uid;
    private String nome;
    private String email;
    private String cidade;
    private String assunto;
    private String mensagem;
    private long timestamp;

    public Sugestao() {
    }

    public Sugestao(String uid, String nome, String email, String cidade, String assunto, String mensagem, long timestamp) {
        this.uid = uid;
        this.nome = nome;
        this.email = email;
        this.cidade = cidade;
        this.assunto = assunto;
        this.mensagem = mensagem;
        this.timestamp = timestamp;
    }

    public String getUid() {
        return uid;
    }

    public String getNome() {
        return nome;
    }

    public String getEmail() {
        return email;
    }

    public String getCidade() {
        return cidade;
    }

    public String getAssunto() {
        return assunto;
    }

    public String getMensagem() {
        return mensagem;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Task<Void> saveOnFirebase() {
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference().child("sugestoes");
        return ref.child(timestamp + "_" + uid).setValue(this);
    }
}
