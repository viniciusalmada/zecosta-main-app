package com.viniciusalmada.tocomzecosta.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.design.button.MaterialButton;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.google.android.gms.tasks.Task;
import com.viniciusalmada.tocomzecosta.R;
import com.viniciusalmada.tocomzecosta.domains.Apoiador;
import com.viniciusalmada.tocomzecosta.utils.EmailUtils;
import com.viniciusalmada.tocomzecosta.utils.NetworkUtils;
import com.viniciusalmada.tocomzecosta.utils.PrefsUtils;
import com.viniciusalmada.tocomzecosta.utils.ToastUtils;
import com.viniciusalmada.tocomzecosta.utils.ViewsUtils;

import java.util.Objects;
import java.util.UUID;

import static com.viniciusalmada.tocomzecosta.activities.BaseActivity.KEY_TYPE_TO_REGISTER;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.KEY_AGE_INT;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.KEY_CITY_INDEX_INT;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.KEY_DDD_INT;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.KEY_EMAIL_STRING;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.KEY_GENDER_INDEX;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.KEY_NOME_STRING;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.KEY_PHONE_LONG;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.KEY_REGISTER_IS_DONE_BOOLEAN;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.KEY_SECAO_LONG;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.KEY_TIMESTAMP;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.KEY_TITULO_LONG;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.KEY_UUID;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.KEY_ZONA_LONG;
import static com.viniciusalmada.tocomzecosta.utils.PrefsUtils.getLong;

public class RegisterActivity extends AppCompatActivity {
    public static final String SPACE = "";
    public static final int DDD_LENGTH = 2;
    public static final int PHONE_LENGHT = 9;
    private EditText mEtNome;
    private EditText mEtAge;
    private RadioGroup mRgGender;
    private EditText mEtEmail;
    private EditText mEtDDD;
    private EditText mEtPhone;
    private Spinner mSpCities;
    private EditText mEtTitulo;
    private EditText mEtSecao;
    private EditText mEtZona;
    private Button mBtRegister;

    private int mCityIndex;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register2);

        setUpToolbar();
        initViews();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        initViewsFromPrefs();
    }

    // Initial commands

    private void setUpToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.activity_register);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void initViewsFromPrefs() {
        String nomePrefs = PrefsUtils.getString(this, PrefsUtils.KEY_NOME_STRING);
        int agePrefs = PrefsUtils.getInt(this, PrefsUtils.KEY_AGE_INT);
        int indexGenderPrefs = PrefsUtils.getInt(this, KEY_GENDER_INDEX);
        String emailPrefs = PrefsUtils.getString(this, PrefsUtils.KEY_EMAIL_STRING);
        int dddPrefs = PrefsUtils.getInt(this, PrefsUtils.KEY_DDD_INT);
        long phonePrefs = PrefsUtils.getLong(this, PrefsUtils.KEY_PHONE_LONG);
        int cityIndexPrefs = PrefsUtils.getInt(this, PrefsUtils.KEY_CITY_INDEX_INT);
        long tituloPrefs = PrefsUtils.getLong(this, PrefsUtils.KEY_TITULO_LONG);
        long secaoPrefs = PrefsUtils.getLong(this, PrefsUtils.KEY_SECAO_LONG);
        long zonaPrefs = PrefsUtils.getLong(this, PrefsUtils.KEY_ZONA_LONG);

        if (!nomePrefs.equals(SPACE)) mEtNome.setText(nomePrefs);
        if (agePrefs != -1) mEtAge.setText(String.valueOf(agePrefs));
        if (indexGenderPrefs != -1) {
            if (indexGenderPrefs == 0)
                ((RadioButton) findViewById(R.id.rb_male)).setChecked(true);
            else if (indexGenderPrefs == 1)
                ((RadioButton) findViewById(R.id.rb_female)).setChecked(true);
            else if (indexGenderPrefs == 2)
                ((RadioButton) findViewById(R.id.rb_gender_other)).setChecked(true);
        }
        if (!emailPrefs.equals(SPACE)) mEtEmail.setText(emailPrefs);
        if (dddPrefs != -1) mEtDDD.setText(String.valueOf(dddPrefs));
        if (phonePrefs != -1) mEtPhone.setText(String.valueOf(phonePrefs));
        if (cityIndexPrefs != -1) mSpCities.setSelection(cityIndexPrefs);
        if (tituloPrefs != -1) mEtTitulo.setText(String.valueOf(tituloPrefs));
        if (secaoPrefs != -1) mEtSecao.setText(String.valueOf(secaoPrefs));
        if (zonaPrefs != -1) mEtZona.setText(String.valueOf(zonaPrefs));

        Intent intent = getIntent();
        if (intent != null){
            boolean isToRegister = intent.getBooleanExtra(KEY_TYPE_TO_REGISTER, false);
            if (isToRegister){
                mBtRegister.setText(R.string.send);
                ((MaterialButton) mBtRegister).setIcon(ContextCompat.getDrawable(this,R.drawable.ic_enviar));
                mBtRegister.setOnClickListener(view -> onSendButtonClick());
            } else {
                mBtRegister.setText(R.string.save);
                ((MaterialButton) mBtRegister).setIcon(ContextCompat.getDrawable(this,R.drawable.ic_salvar));
                mBtRegister.setOnClickListener(view -> onUpdateButtonClick());
            }

        }
    }

    private void initViews() {
        mEtNome = findViewById(R.id.et_reg_nome);
        mEtAge = findViewById(R.id.et_reg_age);
        mRgGender = findViewById(R.id.rg_reg_gender);
        mEtEmail = findViewById(R.id.et_reg_email);
        mEtDDD = findViewById(R.id.et_reg_ddd);
        mEtPhone = findViewById(R.id.et_reg_phone);
        mSpCities = findViewById(R.id.sp_reg_cities);
        mEtTitulo = findViewById(R.id.et_reg_titulo);
        mEtSecao = findViewById(R.id.et_reg_secao);
        mEtZona = findViewById(R.id.et_reg_zona);
        mBtRegister = findViewById(R.id.bt_reg_send);
    }

    // Listeners

    private void onSendButtonClick() {
        showProgress();

        hideErrorsOfTextInputLayouts();

        String nome = checkEditTextAndGetContent(mEtNome, R.id.til_reg_nome);
        String age = checkEditTextAndGetContent(mEtAge, R.id.til_reg_age);
        String gender = getRadioGroupGenderContent(mRgGender);
        String email = checkEmailEditTextAndGetContent(mEtEmail, R.id.til_reg_email);
        String ddd = checkDDDEditTextAndGetContent(mEtDDD, R.id.til_reg_ddd);
        String phone = checkPhoneEditTextAndGetContent(mEtPhone, R.id.til_reg_phone);
        String city = getCityOfSpinner(mSpCities);
        String titulo = checkEditTextAndGetContent(mEtTitulo, R.id.til_reg_titulo);
        String secao = checkEditTextAndGetContent(mEtSecao, R.id.til_reg_secao);
        String zona = checkEditTextAndGetContent(mEtZona, R.id.til_reg_zona);
        String uid = UUID.randomUUID().toString();
        long timestamp = System.currentTimeMillis();

        if (!nome.equals(SPACE) &&
                !age.equals(SPACE) &&
                !email.equals(SPACE) &&
                !ddd.equals(SPACE) &&
                !phone.equals(SPACE) &&
                !titulo.equals(SPACE) &&
                !secao.equals(SPACE) &&
                !zona.equals(SPACE)) {

            ViewsUtils.setViewsDisabled(mEtNome, mEtAge, mRgGender, mEtEmail, mEtDDD, mEtPhone, mSpCities, mEtTitulo, mEtSecao, mEtZona);

            Apoiador apoiador = getApoiador(nome, age, gender, email, ddd, phone, city, titulo, secao, zona, uid, timestamp);
            if (NetworkUtils.isNetworkAvailable(this)) {
                apoiador.saveOnFirebase().addOnCompleteListener(task -> onSaveApoiador(task, apoiador));
            } else {
                ViewsUtils.setViewsEnabled(mEtNome, mEtAge, mRgGender, mEtEmail, mEtDDD, mEtPhone, mSpCities, mEtTitulo, mEtSecao, mEtZona);
                ToastUtils.show(this, "Erro! Verifique sua conexão e tente mais tarde!");
                hideProgress();
            }
        } else {
            hideProgress();
        }
    }

    private void onUpdateButtonClick() {
        showProgress();

        hideErrorsOfTextInputLayouts();

        String nome = checkEditTextAndGetContent(mEtNome, R.id.til_reg_nome);
        String age = checkEditTextAndGetContent(mEtAge, R.id.til_reg_age);
        String gender = getRadioGroupGenderContent(mRgGender);
        String email = checkEmailEditTextAndGetContent(mEtEmail, R.id.til_reg_email);
        String ddd = checkDDDEditTextAndGetContent(mEtDDD, R.id.til_reg_ddd);
        String phone = checkPhoneEditTextAndGetContent(mEtPhone, R.id.til_reg_phone);
        String city = getCityOfSpinner(mSpCities);
        String titulo = checkEditTextAndGetContent(mEtTitulo, R.id.til_reg_titulo);
        String secao = checkEditTextAndGetContent(mEtSecao, R.id.til_reg_secao);
        String zona = checkEditTextAndGetContent(mEtZona, R.id.til_reg_zona);
        String uid = PrefsUtils.getString(this, KEY_UUID);
        long timestamp = System.currentTimeMillis();

        if (!nome.equals(SPACE) &&
                !age.equals(SPACE) &&
                !email.equals(SPACE) &&
                !ddd.equals(SPACE) &&
                !phone.equals(SPACE) &&
                !titulo.equals(SPACE) &&
                !secao.equals(SPACE) &&
                !zona.equals(SPACE)) {

            ViewsUtils.setViewsDisabled(mEtNome, mEtAge, mRgGender, mEtEmail, mEtDDD, mEtPhone, mSpCities, mEtTitulo, mEtSecao, mEtZona);
            long oldTimestamp = getLong(this, KEY_TIMESTAMP);

            Apoiador apoiador = getApoiador(nome, age, gender, email, ddd, phone, city, titulo, secao, zona, uid, timestamp);
            if (NetworkUtils.isNetworkAvailable(this)) {
                apoiador.updateOnFirebase(oldTimestamp).addOnCompleteListener(task -> onSaveApoiador(task, apoiador));
            } else {
                ViewsUtils.setViewsEnabled(mEtNome, mEtAge, mRgGender, mEtEmail, mEtDDD, mEtPhone, mSpCities, mEtTitulo, mEtSecao, mEtZona);
                ToastUtils.show(this, "Erro! Verifique sua conexão e tente mais tarde!");
                hideProgress();
            }
        } else {
            hideProgress();
        }
    }

    private void onSaveApoiador(Task<Void> task, Apoiador apoiador) {
        if (task.isSuccessful()) {
            ViewsUtils.setViewsClearAndEnabled(mEtNome, mEtAge, mRgGender, mEtEmail, mEtDDD, mEtPhone, mSpCities, mEtTitulo, mEtSecao, mEtZona);
            ToastUtils.show(this, "Cadastro realizado! Obrigado!");
            PrefsUtils.setString(this, KEY_NOME_STRING, apoiador.getNome());
            PrefsUtils.setInt(this, KEY_AGE_INT, apoiador.getIdade());
            switch (apoiador.getSexo()) {
                case "Masculino":
                    PrefsUtils.setInt(this, KEY_GENDER_INDEX, 0);
                    break;
                case "Feminino":
                    PrefsUtils.setInt(this, KEY_GENDER_INDEX, 1);
                    break;
                default:
                    PrefsUtils.setInt(this, KEY_GENDER_INDEX, 2);
                    break;
            }
            PrefsUtils.setString(this, KEY_EMAIL_STRING, apoiador.getEmail());
            PrefsUtils.setInt(this, KEY_DDD_INT, apoiador.getDDD());
            PrefsUtils.setLong(this, KEY_PHONE_LONG, apoiador.getPhoneNumber());
            PrefsUtils.setInt(this, KEY_CITY_INDEX_INT, mCityIndex);
            PrefsUtils.setLong(this, KEY_TITULO_LONG, apoiador.getTitulo());
            PrefsUtils.setLong(this, KEY_SECAO_LONG, apoiador.getSection());
            PrefsUtils.setLong(this, KEY_ZONA_LONG, apoiador.getZona());
            PrefsUtils.setString(this, KEY_UUID, apoiador.getUid());
            PrefsUtils.setLong(this, KEY_TIMESTAMP, apoiador.getTimestamp());

            PrefsUtils.setBoolean(this, KEY_REGISTER_IS_DONE_BOOLEAN, true);
            hideProgress();
            finish();
        } else {
            ViewsUtils.setViewsEnabled(mEtNome, mEtAge, mRgGender, mEtEmail, mEtDDD, mEtPhone, mSpCities, mEtTitulo, mEtSecao, mEtZona);
            ToastUtils.show(this, "Erro! Verifique sua conexão e tente mais tarde!");
        }
        hideProgress();
    }

    // Progress visibility control

    private void showProgress() {
        findViewById(R.id.progress).setVisibility(View.VISIBLE);
    }

    private void hideProgress() {
        findViewById(R.id.progress).setVisibility(View.INVISIBLE);
    }

    // Checkers and Getters from Form

    private String checkEditTextAndGetContent(EditText field, @IdRes int idTextInputLayout) {
        if (field.getText().toString().trim().length() != 0) {
            return field.getText().toString();
        } else {
            showErrorEditText(idTextInputLayout);
            return SPACE;
        }
    }

    private String checkEmailEditTextAndGetContent(EditText emailField, int idTextInputLayout) {
        String email = checkEditTextAndGetContent(emailField, idTextInputLayout);
        if (!email.equals(SPACE) && !EmailUtils.checkEmail(email)) {
            showErrorEditText(idTextInputLayout, "Email incorreto");
            return SPACE;
        }
        if (!email.equals(SPACE)) {
            PrefsUtils.setString(this, KEY_EMAIL_STRING, email);
            return email;
        }
        return SPACE;
    }

    private String checkDDDEditTextAndGetContent(EditText dddField, int idTextInputLayout) {
        String ddd = checkEditTextAndGetContent(dddField, idTextInputLayout);
        if (!ddd.equals(SPACE) && ddd.length() != DDD_LENGTH) {
            showErrorEditText(idTextInputLayout, "DDD incompleto");
            return SPACE;
        } else if (!ddd.equals(SPACE) && ddd.length() == DDD_LENGTH) {
            return ddd;
        } else {
            return SPACE;
        }
    }

    private String checkPhoneEditTextAndGetContent(EditText phoneField, int idTextInputLayout) {
        String phone = checkEditTextAndGetContent(phoneField, idTextInputLayout);
        if (!phone.equals(SPACE) && phone.length() != PHONE_LENGHT) {
            showErrorEditText(idTextInputLayout, "Número incompleto");
            return SPACE;
        } else if (!phone.equals(SPACE) && phone.length() == PHONE_LENGHT) {
            return phone;
        } else {
            return SPACE;
        }
    }

    private String getRadioGroupGenderContent(RadioGroup radioGroup) {
        String[] gender = getResources().getStringArray(R.array.genders);
        int id = radioGroup.getCheckedRadioButtonId();
        switch (id) {
            case R.id.rb_male:
                return gender[0];
            case R.id.rb_female:
                return gender[1];
            case R.id.rb_gender_other:
                return gender[2];
        }
        return null;
    }

    private String getCityOfSpinner(Spinner spCity) {
        String[] cities = getResources().getStringArray(R.array.maranhao_cities);
        mCityIndex = spCity.getSelectedItemPosition();
        return cities[spCity.getSelectedItemPosition()];
    }

    // Errors controls

    private void hideErrorsOfTextInputLayouts() {
        ((TextInputLayout) findViewById(R.id.til_reg_nome)).setErrorEnabled(false);
        ((TextInputLayout) findViewById(R.id.til_reg_email)).setErrorEnabled(false);
        ((TextInputLayout) findViewById(R.id.til_reg_age)).setErrorEnabled(false);
        ((TextInputLayout) findViewById(R.id.til_reg_ddd)).setErrorEnabled(false);
        ((TextInputLayout) findViewById(R.id.til_reg_phone)).setErrorEnabled(false);
        ((TextInputLayout) findViewById(R.id.til_reg_titulo)).setErrorEnabled(false);
        ((TextInputLayout) findViewById(R.id.til_reg_secao)).setErrorEnabled(false);
        ((TextInputLayout) findViewById(R.id.til_reg_zona)).setErrorEnabled(false);
    }

    private void showErrorEditText(@IdRes int idTextInputLayout) {
        TextInputLayout til = findViewById(idTextInputLayout);
        til.setError(getString(R.string.error_text));
    }

    private void showErrorEditText(@IdRes int idTextInputLayout, String error) {
        TextInputLayout til = findViewById(idTextInputLayout);
        til.setError(error);
    }

    private Apoiador getApoiador(String nome, String age, String gender, String email, String ddd, String phone, String city, String titulo, String secao, String zona, String uid, long timestamp) {
        long longPhone = Long.parseLong(ddd.concat(phone));
        long intZona = Long.parseLong(zona);
        long intSecao = Long.parseLong(secao);
        long longTitulo = Long.parseLong(titulo);
        int intAge = Integer.parseInt(age);
        return new Apoiador(city, email, longPhone, intZona, intSecao, longTitulo, nome, intAge, gender, uid, timestamp);
    }

}
