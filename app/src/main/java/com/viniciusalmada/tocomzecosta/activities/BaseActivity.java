package com.viniciusalmada.tocomzecosta.activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.viniciusalmada.tocomzecosta.R;
import com.viniciusalmada.tocomzecosta.fragments.HistoryFragment;
import com.viniciusalmada.tocomzecosta.fragments.SugestaoFragment;
import com.viniciusalmada.tocomzecosta.utils.PrefsUtils;

@SuppressLint("Registered")
public class BaseActivity extends AppCompatActivity {
    public static final String KEY_TYPE_TO_REGISTER = "KEY_TYPE_TO_REGISTER";
    protected DrawerLayout mDrawerLayout;
    protected NavigationView mNavigationView;

    protected void setUpToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolbar.setTitle(R.string.app_title);
            toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.secondaryTextColor));
            setSupportActionBar(toolbar);
        }
    }

    protected void setUpNavDrawer() {
        final ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setHomeAsUpIndicator(R.drawable.ic_menu);
            bar.setDisplayHomeAsUpEnabled(true);
        }

        mDrawerLayout = findViewById(R.id.drawer);
        mNavigationView = findViewById(R.id.nv_main);
        View header = mNavigationView.getHeaderView(0);
        if (PrefsUtils.getBoolean(this, PrefsUtils.KEY_REGISTER_IS_DONE_BOOLEAN)) {
            showRegisteredLayout(header);
        } else {
            hideRegisterLayout(header);
        }
        if (mDrawerLayout != null) {
            mNavigationView.setNavigationItemSelectedListener((menuItem) -> {
                menuItem.setChecked(true);
                mDrawerLayout.closeDrawers();
                onNavDrawerItemSelected(menuItem);
                return true;
            });
            Fragment fragment = new HistoryFragment();
            FragmentManager fm = getSupportFragmentManager();
            fm.beginTransaction().replace(R.id.fl_fragment, fragment).commit();
        }
        openDrawer();
    }

    private void showRegisteredLayout(View header) {
        header.findViewById(R.id.bt_register_apoiador).setVisibility(View.GONE);
        header.findViewById(R.id.ll_register).setVisibility(View.VISIBLE);
        String nome = PrefsUtils.getString(this, PrefsUtils.KEY_NOME_STRING);
        String email = PrefsUtils.getString(this, PrefsUtils.KEY_EMAIL_STRING);
        int ddd = PrefsUtils.getInt(this, PrefsUtils.KEY_DDD_INT);
        long numPhone = PrefsUtils.getLong(this, PrefsUtils.KEY_PHONE_LONG);
        String phone = "(".concat(String.valueOf(ddd)).concat(") ").concat(String.valueOf(numPhone));

        ((TextView) header.findViewById(R.id.tv_apo_nome)).setText(nome);
        ((TextView) header.findViewById(R.id.tv_apo_email)).setText(email);
        ((TextView) header.findViewById(R.id.tv_apo_telefone)).setText(phone);

        header.findViewById(R.id.bt_apo_edit_register).setOnClickListener(view -> {
            Intent intent = new Intent(this, RegisterActivity.class);
            intent.putExtra(KEY_TYPE_TO_REGISTER, false);
            startActivity(intent);
        });
    }

    private void hideRegisterLayout(View header) {
        header.findViewById(R.id.bt_register_apoiador).setVisibility(View.VISIBLE);
        header.findViewById(R.id.ll_register).setVisibility(View.GONE);
        header.findViewById(R.id.bt_register_apoiador).setOnClickListener(view -> {
            Intent intent = new Intent(this, RegisterActivity.class);
            intent.putExtra(KEY_TYPE_TO_REGISTER, true);
            startActivity(intent);
        });
    }

    private View.OnClickListener onRegisterButtonClick() {
        return (v -> {
            Intent intent = new Intent(getBaseContext(), RegisterActivity.class);
            startActivity(intent);
        });
    }

    private void onNavDrawerItemSelected(MenuItem menuItem) {
        Fragment fragment = new HistoryFragment();

        switch (menuItem.getItemId()) {
            case R.id.nav_item_historia:
                fragment = new HistoryFragment();
                break;
            case R.id.nav_item_galeria:
                break;
            case R.id.nav_item_projetos:
                break;
            case R.id.nav_item_participe:
                break;
            case R.id.nav_item_suggestions:
                fragment = new SugestaoFragment();
                break;
        }

        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().replace(R.id.fl_fragment, fragment).commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                if (mDrawerLayout != null) {
                    openDrawer();
                    return true;
                }
        }
        return super.onOptionsItemSelected(item);
    }

    public void openDrawer() {
        if (mDrawerLayout != null) mDrawerLayout.openDrawer(GravityCompat.START);
    }

    protected void closeDrawer() {
        if (mDrawerLayout != null) mDrawerLayout.closeDrawer(GravityCompat.START);
    }

    public void setToolbarTitle(int text) {
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle(text);
    }
}
