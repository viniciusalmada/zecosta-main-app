package com.viniciusalmada.tocomzecosta.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;

import com.viniciusalmada.tocomzecosta.R;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpToolbar();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpNavDrawer();
    }

    @Override
    public void onBackPressed() {
        if (!mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            openDrawer();
        } else {
            super.onBackPressed();
        }
    }
}
